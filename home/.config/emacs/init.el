;;; init.el --- Emacs init file --- -*- lexical-binding: t; -*-

;;; Commentary:

;; This file is a part of the dotfiles repository.
;; This file should be symlinked as ~/.config/emacs/init.el.

;;; Code:

;; ------------------------------------ ;;
;; Set up built-in modes and variables. ;;
;; ------------------------------------ ;;

;; Show cursor's line and column number in the status bar.
(column-number-mode 1)
;; Delete seleted text when typing.
(delete-selection-mode 1)
;; Restore open files on Emacs start.
(desktop-save-mode 1)
;; Auto-indent on【Enter】.
(electric-indent-mode 1)
;; Auto-insert/close bracket pairs.
(electric-pair-mode 1)
;; Keep a list of recently opened files.
(recentf-mode 1)
;; Reload file if it was modified by an external program.
(global-auto-revert-mode 1)
;; Turn on syntax coloring.
(global-font-lock-mode 1)
;; Paren match highlighting.
(show-paren-mode 1)
;; Disable toolbar.
(tool-bar-mode 0)
;; Highlight text selection.
(transient-mark-mode 1)

;; Allow downcasing by "C-x C-l".
(put 'downcase-region 'disabled nil)
;; Allow downcasing by "C-x C-u".
(put 'upcase-region 'disabled nil)

;; Allow narrowing by "C-x n n".
;; Widen back by "C-x n w".
(put 'narrow-to-region 'disabled nil)

;; Backups.
(setq backup-by-copying t)
;; Directory for backup~ files.
(setq backup-directory-alist '(("." . "~/.local/state/emacs/backups")))

;; Ibuffer.
(autoload 'ibuffer "ibuffer" "List buffers." t)
(setq ibuffer-default-sorting-mode 'filename/process)
(global-set-key (kbd "C-x C-b") 'ibuffer)

;; Indentation.
(setq-default tab-width 4)
(setq-default indent-tabs-mode nil)
(setq-default sh-basic-offset 2)


;; ---------------------------------------------- ;;
;; Set up packages installed from ELPA and MELPA. ;;
;; ---------------------------------------------- ;;

;; Set up repositories and the "use-package" package.

(require 'package)
(package-initialize)
(add-to-list 'package-archives
             '("melpa" . "http://melpa.org/packages/") t)

(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))
(require 'use-package)
(setq use-package-always-ensure t)

;; Set up other packages from the repository using "use-package".

;; AsciiDoc mode.
(use-package adoc-mode
  :mode "\\.adoc\\'")

;; ;; Auto-complete mode.
;; (use-package auto-complete
;;   :config (global-auto-complete-mode 1))

;; Auto-complete C headers.
(use-package auto-complete-c-headers
  :config (add-to-list 'ac-sources 'ac-source-c-headers))

;; Bats is Bash Automated Testing System.
(use-package bats-mode
  :mode "\\.bats\\'")

;; Clang-format is a package for code reformatting
;; using the clang-format utility.
(use-package clang-format
  :commands (clang-format-buffer clang-format-region clang-format))

;; Company mode.
(use-package company
  :hook (scala-mode . company-mode)
  :config
  (global-company-mode 1)
  (setq lsp-completion-provider :capf))

;; Darcula theme.
(use-package darcula-theme
  :ensure t
  :config (set-frame-font "DejaVu Sans Mono-18"))
(load-theme 'darcula t)

;; Dockerfile mode.
(use-package dockerfile-mode
  :mode "/Dockerfile[^/]*\\'")

;; Highlight uncommitted changes.
(use-package diff-hl
  :config (global-diff-hl-mode))

;; Enable nice rendering of diagnostics like compile errors.
(use-package flycheck
  :init
  (global-flycheck-mode)
  (setq-default flycheck-shellcheck-excluded-warnings '("SC1083,SC1089,SC2154")))

;; Graphviz dot mode.
(use-package graphviz-dot-mode)

;; Groovy mode.
(use-package groovy-mode)

;; Haskell mode.
(use-package haskell-mode)

;; Edit multiple regions in the same way simultaneously.
(use-package iedit)

;; LSP for Scala.
(use-package lsp-mode
  :init (setq lsp-prefer-flymake nil)
  :hook
  (scala-mode . lsp)
  (lsp-mode . lsp-lens-mode)
  :config (setq lsp-prefer-flymake nil))

(use-package lsp-metals
  :after scala-mode
  :demand t)

(use-package lsp-ui)

;; Highlight the previously visible buffer part after each scroll.
(use-package on-screen
  :config (on-screen-global-mode 1))

;; Preproc Font Lock is an Emacs package that highlight C-style
;; preprocessor directives. The main feature is support for macros
;; that span multiple lines.
(use-package preproc-font-lock
  :config (preproc-font-lock-global-mode 1))

;; Highlight brackets according to their depth
(use-package rainbow-delimiters
  :hook (prog-mode . rainbow-delimiters-mode))

;; Handling of comment boxes in various styles.
(use-package rebox2
  :bind ("M-Q" . rebox-cycle)
  :custom (rebox-style-loop '(21 23 25 27)))

;; Enable scala-mode and sbt-mode.
(use-package scala-mode
  :mode "\\.s\\(cala\\|bt\\)$")

(use-package sbt-mode
  :commands sbt-start sbt-command
  :config
  ;; WORKAROUND: https://github.com/ensime/emacs-sbt-mode/issues/31
  ;; allows using SPACE when in the minibuffer
  (substitute-key-definition
   'minibuffer-complete-word
   'self-insert-command
   minibuffer-local-completion-map))

;; Change cursor color dynamically.
(use-package smart-cursor-color
  :config (smart-cursor-color-mode 1))

;; String-inflection provides
;; "underscore -> UPCASE -> CamelCase -> lowerCamelCase"
;; conversion of names.
(use-package string-inflection
  :bind ("C-c _" . string-inflection-all-cycle))

;; Undo-tree.
(use-package undo-tree
  :config
  (global-undo-tree-mode 1)
  (setq undo-tree-auto-save-history nil)
  (defalias 'undo 'undo-tree-undo)
  (defalias 'redo 'undo-tree-redo)
  :bind ("C-z"   . undo) ; Undo 【Ctrl+z】
  :bind ("C-S-z" . redo) ; Redo 【Ctrl+Shift+z】
  )

;; Unobtrusively remove trailing whitespace.
(use-package ws-butler
  :config (ws-butler-global-mode 1))

;; YAML mode.
(use-package yaml-mode
  :mode "\\.ksy\\'"
  :mode "\\.yml\\'")


;; ---------------------------------- ;;
;; Set up manually-installed scripts. ;;
;; ---------------------------------- ;;

(add-to-list 'load-path "~/.config/emacs/lisp")

;; Indentation for C/C++.
;; (setq c-default-style "bsd")
(setq c-default-style "stroustrup")
(setq c-basic-offset 4)
;; (defun do-not-indent-in-namespace () (c-set-offset 'innamespace [0]))
;; (add-hook 'c-mode-common-hook 'do-not-indent-in-namespace)
;; (add-hook 'c-mode-common-hook (lambda () (c-set-offset 'case-label '+)))
;; (smart-tabs-insinuate 'c 'c++ 'java 'javascript 'cperl 'ruby 'nxml)
(add-to-list 'auto-mode-alist '("\\.h\\'"   . c++-mode))
(add-to-list 'auto-mode-alist '("\\.inc\\'" . c++-mode))

;; (require 'setup-cedet)

;; Jenkinsfile mode.
(when (require 'jenkinsfile-mode nil :noerror)
  ;;(autoload 'jenkinsfile-mode "jenkinsfile-mode" "Major mode for Jenkinsfiles" t)
  (add-to-list 'auto-mode-alist '("^Jenkinsfile" . jenkinsfile-mode)))

;; Smali/baksmali mode.
(when (require 'smali-mode nil :noerror)
  ;;(autoload 'smali-mode "smali-mode" "Major mode for editing and viewing smali issues" t)
  (add-to-list 'auto-mode-alist '("\\.smali\\'" . smali-mode)))


;; ---------------------------------------------- ;;
;; Set up variables using the "Custom" mechanism. ;;
;; ---------------------------------------------- ;;

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   '(adoc-mode auto-complete-c-headers bats-mode clang-format diff-hl fill-column-indicator graphviz-dot-mode groovy-mode haskell-mode iedit markdown-mode on-screen preproc-font-lock protobuf-mode rainbow-delimiters rebox2 smart-cursor-color smart-tabs-mode string-inflection undo-tree use-package ws-butler yaml-mode)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
