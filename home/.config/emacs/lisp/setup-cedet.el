(require 'cc-mode)
(require 'semantic)
(require 'semantic/ia)

;; Semantic

;; select which submodes we want to activate
(add-to-list 'semantic-default-submodes 'global-semantic-mru-bookmark-mode)
(add-to-list 'semantic-default-submodes 'global-semanticdb-minor-mode)
(add-to-list 'semantic-default-submodes 'global-semantic-idle-scheduler-mode)
(add-to-list 'semantic-default-submodes 'global-semantic-highlight-func-mode)

(global-semanticdb-minor-mode 1)
(global-semantic-idle-scheduler-mode 1)
(global-semantic-stickyfunc-mode 0)
(global-semantic-idle-completions-mode 1)
(global-semantic-decoration-mode 0)
(global-semantic-highlight-func-mode 1)
(global-semantic-show-unmatched-syntax-mode 0)

(semantic-mode 1)

(defun alexott/cedet-hook ()
  (local-set-key "\C-c\C-j" 'semantic-ia-fast-jump)
  (local-set-key "\C-c\C-s" 'semantic-ia-show-summary))

(add-hook 'c-mode-common-hook 'alexott/cedet-hook)
(add-hook 'c-mode-hook        'alexott/cedet-hook)
(add-hook 'c++-mode-hook      'alexott/cedet-hook)


;; EDE.

(require 'ede)
(global-ede-mode)
(provide 'setup-cedet)
