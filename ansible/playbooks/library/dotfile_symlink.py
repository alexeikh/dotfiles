#!/usr/bin/python3
# -*- coding: utf-8 -*-

# Copyright: (c) 2020, Alexei Khlebnikov <alexei.khlebnikov@gmail.com>
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)

ANSIBLE_METADATA = {
    'metadata_version': '1.1',
    'status': ['preview'],
    'supported_by': 'community'
}

DOCUMENTATION = '''
---
module: dotfiles_symlink

short_description: Create a symlink to a dotfile

version_added: "2.4"

description:
  - >
    Create a relative symlink to a dotfile.
    Backup file or directory possibly existing at the same location.

options:
  path:
    description:
      - >
        Path to the symlink.
        Eventual existing file/directory at `path` will be backed up as `path.YYYY-MM-DD`
        or as `path.YYYY-MM-DD.NNN` if `path.YYYY-MM-DD` already exists.
    required: true
    type: path
  link_target:
    description:
      - >
        Path to the symlink target, i.e. file or directory to point to.
        Either full path or relative to the symlink path.
    required: true
    type: path

author:
    - Alexei Khlebnikov
'''

EXAMPLES = '''
# Make symlink
- name: Make symlink
  path: /path/to/symlink
  link_target: /path/to/linked/file
'''

RETURN = '''
path:
    description: The original `path` argument that was passed in.
    type: str
    returned: always
link_target:
    description: The original `link_target` argument that was passed in.
    type: str
    returned: always
relative_link_target:
    description: Relative link target, calculated based on `path` and `link_target`. It will be the actual link target of the symlink.
    type: str
    returned: always
'''

import re
import shutil

from functools import cmp_to_key
from glob import glob
from itertools import filterfalse
from os import makedirs, readlink, remove, rename, symlink
from os.path import exists, join, dirname, isdir, isfile, islink, lexists, realpath, relpath
from time import strftime

from ansible.errors import AnsibleError
from ansible.module_utils.basic import AnsibleModule


class DotfileSymlinker:
    def __init__(self, module):
        self.module = module

    def resolve_link_target(self, symlink_dirname, link_target):
        return join(symlink_dirname, link_target)

    def resolve_link_target_realpath(self, symlink_dirname, link_target):
        resolved_link_target = self.resolve_link_target(symlink_dirname, link_target)
        return realpath(resolved_link_target)

    def resolve_relative_link_target(self, symlink_dirname, link_target):
        resolved_link_target = self.resolve_link_target(symlink_dirname, link_target)
        return relpath(resolved_link_target, start=symlink_dirname)

    def find_free_backup_path(self, symlink_path):
        date_string = strftime('%Y-%m-%d')
        dated_path = f'{symlink_path}.{date_string}'
        if not lexists(dated_path):
            return dated_path

        number = 2
        while True:
            dated_numbered_path = f'{dated_path}.{number:03}'
            if lexists(dated_numbered_path):
                number += 1
                if number > 999:
                    raise AnsibleError(f'Too many backups exist for path: {symlink_path}')
            else:
                return dated_numbered_path

    def backup(self, symlink_path):
        backup_path = self.find_free_backup_path(symlink_path)
        rename(symlink_path, backup_path)

    def process(self):
        module = self.module
        symlink_path = module.params['path']
        link_target = module.params['link_target']

        symlink_dirname = dirname(symlink_path)
        relative_link_target = self.resolve_relative_link_target(symlink_dirname, link_target)

        result = dict(
            changed=False,
            path=symlink_path,
            link_target=link_target,
            relative_link_target=relative_link_target,
        )

        link_target_realpath = self.resolve_link_target_realpath(symlink_dirname, link_target)
        if not exists(link_target_realpath):
            msg = f'Final link target, {link_target_realpath}, does not exist'
            module.fail_json(msg=msg, **result)

        # If the correct link is already present - nothing to do.
        if islink(symlink_path) and readlink(symlink_path) == relative_link_target:
            module.exit_json(**result)

        # Changes will be made.
        result['changed'] = True
        if module.check_mode:
            module.exit_json(**result)

        # Free up the symlink location.
        if islink(symlink_path):
            remove(symlink_path)
        elif exists(symlink_path):
            self.backup(symlink_path)

        makedirs(symlink_dirname, exist_ok=True)
        symlink(relative_link_target, symlink_path)
        module.exit_json(**result)


def run_module():
    # define available arguments/parameters a user can pass to the module
    module_args = dict(
        path=dict(type='path', required=True),
        link_target=dict(type='path', required=True),
    )

    module = AnsibleModule(
        argument_spec=module_args,
        supports_check_mode=True
    )

    processor = DotfileSymlinker(module)
    processor.process()


def main():
    run_module()


if __name__ == '__main__':
    main()
