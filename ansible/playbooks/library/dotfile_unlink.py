#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright: (c) 2020, Alexei Khlebnikov <alexei.khlebnikov@gmail.com>
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)

ANSIBLE_METADATA = {
    'metadata_version': '1.1',
    'status': ['preview'],
    'supported_by': 'community'
}

DOCUMENTATION = '''
---
module: dotfiles_unlink

short_description: Remove a symlink to a dotfile

version_added: "2.4"

description:
  - >
    Remove a symlink to a dotfile.
    Restore eventual previous file/directory at the same location, if it was backed up.

options:
  path:
    description:
      - >
        Path to the symlink.
        If a backup named as `path.YYYY-MM-DD` or `path.YYYY-MM-DD.NNN` exists,
        the previously existing file/directory at `path` will be restored
        from the latest backup.
    required: true
    type: path

author:
    - Alexei Khlebnikov
'''

EXAMPLES = '''
# Remove symlink
- name: Remove symlink
  path: /path/to/symlink
'''

RETURN = '''
path:
    description: The original `path` param that was passed in.
    type: str
    returned: always
'''

import re
import shutil

from functools import cmp_to_key
from glob import glob
from itertools import filterfalse
from os import readlink, remove, rename, symlink
from os.path import dirname, exists, join, isdir, isfile, islink, lexists, realpath, relpath
from time import strftime

from ansible.errors import AnsibleError
from ansible.module_utils.basic import AnsibleModule


class DotfileUnlinker:
    def __init__(self, module):
        self.module = module

    def find_latest_backup_path(self, symlink_path):
        # If symlink points to a valid file or directory - just use it as a backup path.
        if lexists(symlink_path):
            backup_realpath = realpath(symlink_path)
            if isfile(backup_realpath) or isdir(backup_realpath):
                return backup_realpath

        dated_pattern = f'{symlink_path}.[0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9]'
        dated_numbered_pattern = f'{dated_pattern}.[0-9][0-9][0-9]'

        backup_paths = glob(dated_pattern) + glob(dated_numbered_pattern)
        backup_paths.sort(reverse=True)

        for backup_path in backup_paths:
            backup_realpath = realpath(backup_path)
            # Only recognise regular files and directories as backup paths.
            if isfile(backup_realpath) or isdir(backup_realpath):
                return backup_realpath

        return None

    def process(self):
        module = self.module
        symlink_path = module.params['path']

        result = dict(
            changed=False,
            path=symlink_path,
        )

        # If a file or a directory already exists at symlink_path -
        # nothing to do.
        if exists(symlink_path) and not islink(symlink_path):
            module.exit_json(**result)

        # If nothing exists at symlink_path and there is no backup -
        # nothing to do.
        latest_backup_path = self.find_latest_backup_path(symlink_path)
        if not latest_backup_path:
            module.exit_json(**result)

        # Changes will be made.
        result['changed'] = True
        if module.check_mode:
            module.exit_json(**result)

        if exists(symlink_path):
            # symlink_path is a link because we checked for non-link above.
            # Thus it is safe to remove it.
            assert islink(symlink_path)
            remove(symlink_path)

        # Restore from backup, it it exists.
        if latest_backup_path:
            if isdir(latest_backup_path):
                shutil.copytree(latest_backup_path, symlink_path)
            else:
                shutil.copy2(latest_backup_path, symlink_path)

        module.exit_json(**result)


def run_module():
    # define available arguments/parameters a user can pass to the module
    module_args = dict(
        path=dict(type='path', required=True),
    )

    module = AnsibleModule(
        argument_spec=module_args,
        supports_check_mode=True
    )

    processor = DotfileUnlinker(module)
    processor.process()


def main():
    run_module()


if __name__ == '__main__':
    main()
