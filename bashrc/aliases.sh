if ! command_exists bat && command_exists batcat; then
  alias bat='batcat'
fi

alias diff='diff --color=auto --unified'

alias k='kubectl'

alias glog='git log --graph HEAD origin/HEAD'
alias gshow='git show --pretty=fuller'

alias ad='git add -u'
alias gf='git diff'
alias gfc='gf --cached'
alias gt='git status'

alias hl='highlight --out-format=truecolor'
alias hla='hl --syntax=asm      | less'
alias hlc='hl --syntax=c        | less'
alias hlj='hl --syntax=java     | less'
alias hlm='hl --syntax=make     | less'
alias hlp='hl --syntax=python   | less'
alias hlr='hl --syntax=ruby     | less'
alias hls='hl --syntax=sh       | less'
alias hlx='hl --syntax=xml      | less'
