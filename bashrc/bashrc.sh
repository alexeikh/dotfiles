# shellcheck shell=bash

# Check shell.
if [ -z "$BASH" ] && [ -z "$ZSH_NAME" ]; then
    echo "$0: Only Bash and Zsh are supported by this script." >&2
    return 1
fi

# Determine path to this script file.
if [ -n "$BASH" ]; then
    # This works in Bash, does not work in Zsh, Fish, Mksh and Dash.
    this_file=${BASH_SOURCE[0]}
else
    # This works in Zsh, does not work if sourced in Bash, Fish, Mksh and Dash.
    this_file=$0
fi

# Determine directory of this script file.
this_dir=$(dirname "$this_file")

# Run other scripts from the directory of this script file.
# pushd and popd commands are supported by Bash, Zsh and Fish,
# are not supported by Mksh and Dash.
pushd "$this_dir" >/dev/null || return
    . ./functions.sh
    . ./aliases.sh
    . ./vars.sh

    . ./bloop.sh

    if [ -n "$BASH" ]; then
        . ./bash_completion.sh
    fi
popd >/dev/null || return
