command_exists () {
    command -v "$1" >/dev/null 2>&1
}

mkcd () {
    mkdir -p -- "$1" &&
    cd -P -- "$1"
}
