export BAT_CONFIG_PATH="$HOME/dotfiles/.config/bat/config"
export LESS="--quit-if-one-screen --RAW-CONTROL-CHARS --tabs=4 --no-init"

if [ -z "$JAVA_HOME" ]; then
  if [ -L /etc/alternatives/java ]; then
    export JAVA_HOME=$(dirname $(dirname $(realpath /etc/alternatives/java)))
  fi
fi
